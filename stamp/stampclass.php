<?php

class stampData {
    public $data="";
    public $evidence="";
    public $hash2="";
    public $hash3="";
    public $to="";
    public $reference="";
    public $subject="";
    public $transactionType="";
}

class stampResult {
    public $trxid="";
    public $code="";
    public $message="";
    public $url="";

    public $data="";
    public $evidence="";
    public $hash2="";
    public $hash3="";
    public $to="";
    public $reference="";
    public $subject="";
    public $transactionType="";

    public $from="";
    public $status="";
    public $timestamp="";

    public $blockchaiId="";
    public $blockchainTimestamp = "";
    public $blockchainToken = "";

    public $merkleRootBTC = "";
    public $prefixBTC = "";
    public $sourceIdBTC = "";
    public $typeBTC = "";

    public $merkleRootETC = "";
    public $prefixETC = "";
    public $sourceIdETC = "";
    public $typeETC = "";
    
    public $merkleRootETH = "";
    public $prefixETH = "";
    public $sourceIdETH = "";
    public $typeETH = "";
    
    public $merkleRootLTC = "";
    public $prefixLTC = "";
    public $sourceIdLTC = "";
    public $typeLTC = "";

    public $merkleRootSTP = "";
    public $prefixSTP = "";
    public $sourceIdSTP = "";
    public $typeSTP = "";

}

class stamp { 
    public $token = ''; 

    function log ($aValue) {
        echo ("**");
        echo ($aValue);
        echo ("**");
        
    }    
    private function tagURL($aTagName, $aTagValue) {
        $aTag   =   urlencode($aTagValue);
        $aTag  = $aTagName.'='.$aTag;
        //echo ($aTag);
        return ($aTag);
    }

    function getStamp($type, $value) { 
        $ch = curl_init();
        
        $data = array(
            'evidence' => $value
        );
        
        curl_setopt($ch, CURLOPT_URL, "https://api.stamping.io/getstamp/?".$type."=".$value);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: Basic ".$this->token
        ));
        $response = curl_exec($ch);
        
        //echo ($response);
        curl_close($ch);

        $result = new stampResult;
        $tmp = json_decode($response);
        $result->code =$tmp->code;
        $result->trxid =$tmp->trxid;
        $result->message =$tmp->message;
        $result->url = "http://www.stamping.io/q/?".$tmp->trxid."&lang=es";
        //http://www.stamping.io/i/?
        $result->data = $tmp->result->data;
        $result->evidence = $tmp->result->hash1;
        $result->hash2 = $tmp->result->hash2;
        $result->hash3 = $tmp->result->hash3;
        $result->to = $tmp->result->owner;
        $result->from = $tmp->result->userId;
        $result->status = $tmp->result->status;
        $result->timestamp = $tmp->result->timestamp;
        $result->reference = $tmp->result->reference;
        $result->subject = $tmp->result->resume;
        $result->transactionType = $tmp->result->transactionType;
        
        $result->blockchaiId = $tmp->result->stamperyGet->result[0]->id;
        $result->blockchainTimestamp = $tmp->result->stamperyGet->result[0]->time;
        $result->blockchainToken = $tmp->result->stamperyGet->result[0]->token;

        $result->merkleRootSTP = $tmp->result->merkleRootSTP;
        $result->prefixSTP = $tmp->result->prefixSTP;
        $result->sourceIdSTP = $tmp->result->sourceIdSTP;
        $result->typeSTP = $tmp->result->typeSTP;

        
        $result->merkleRootBTC = $tmp->result->stamperyGet->result[0]->receipts->btc->merkleRoot;
        $result->prefixBTC = $tmp->result->stamperyGet->result[0]->receipts->btc->anchors[0]->prefix;
        $result->sourceIdBTC = $tmp->result->stamperyGet->result[0]->receipts->btc->anchors[0]->sourceId;
        $result->typeBTC = $tmp->result->stamperyGet->result[0]->receipts->btc->anchors[0]->type;
        
        $result->merkleRootETC = $tmp->result->stamperyGet->result[0]->receipts->etc->merkleRoot;
        $result->prefixETC = $tmp->result->stamperyGet->result[0]->receipts->etc->anchors[0]->prefix;
        $result->sourceIdETC = $tmp->result->stamperyGet->result[0]->receipts->etc->anchors[0]->sourceId;
        $result->typeETC = $tmp->result->stamperyGet->result[0]->receipts->etc->anchors[0]->type;
        
        $result->merkleRootETH = $tmp->result->stamperyGet->result[0]->receipts->eth->merkleRoot;
        $result->prefixETH = $tmp->result->stamperyGet->result[0]->receipts->eth->anchors[0]->prefix;
        $result->sourceIdETH = $tmp->result->stamperyGet->result[0]->receipts->eth->anchors[0]->sourceId;
        $result->typeETH = $tmp->result->stamperyGet->result[0]->receipts->eth->anchors[0]->type;
        
        
        $result->merkleRootLTC = $tmp->result->stamperyGet->result[0]->receipts->ltc->merkleRoot;
        $result->prefixLTC = $tmp->result->stamperyGet->result[0]->receipts->ltc->anchors[0]->prefix;
        $result->sourceIdLTC = $tmp->result->stamperyGet->result[0]->receipts->ltc->anchors[0]->sourceId;
        $result->typeLTC = $tmp->result->stamperyGet->result[0]->receipts->ltc->anchors[0]->type;
        
        //$result->data = $tmp->data;
        return ($result);
    }

    function setStamp($stampData) { 
        $ch = curl_init();
        $aUrl  = "";
        $aUrl  = "?".$this->tagURL('evidence',$stampData->evidence);
        if ($stampData->hash2!=="") $aUrl .= "&".$this->tagURL('hash2',$stampData->hash2);
        if ($stampData->hash3!=="") $aUrl .= "&".$this->tagURL('hash3',$stampData->hash3);
        if ($stampData->subject!=="") $aUrl .= "&".$this->tagURL('subject',$stampData->subject);
        if ($stampData->data!=="") $aUrl .= "&".$this->tagURL('data',$stampData->data);
        if ($stampData->to!=="") $aUrl .= "&".$this->tagURL('to',$stampData->to);
        if ($stampData->reference!=="") $aUrl .= "&".$this->tagURL('reference',$stampData->reference);
        if ($stampData->transactionType!=="") $aUrl .= "&".$this->tagURL('transactionType',$stampData->transactionType);
        if ($stampData->timestamp!=="") $aUrl .= "&".$this->tagURL('timestamp',$stampData->timestamp);
        //echo ($aUrl);
        //return 1;
        $data = array(
            'evidence' => $stampData->evidence,
            'hash2' => $stampData->hash2,
            'hash3' => $stampData->hash3,
            'subject' => $stampData->subject,
            'data' => $stampData->data,
            'to' => $stampData->to,
            'reference' => $stampData->reference,
            'transactionType' => $stampData->transactionType,
            'timestamp' => $stampData->timestamp
        );
        curl_setopt($ch, CURLOPT_URL, "https://api.stamping.io/stamp/".$aUrl);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: Basic ".$this->token
        ));
        $response = curl_exec($ch);
        curl_close($ch);

        $result = new stampResult;
        
        $tmp = json_decode($response);
        $result->code =$tmp->code;
        $result->trxid =$tmp->trxid;
        $result->message =$tmp->message;
        $result->url = "http://www.stamping.io/q/?".$tmp->trxid."&lang=en";
        return ($result);
    } 
} 
?>